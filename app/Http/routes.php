<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$app->get('/', function () use ($app) {
    return $app->version();
});

$app->get('/user', 'UserController@getUser');

$app->get('/userdetail', 'UserController@getUserDetail');

$app->get('/kategori', 'KategoriController@getKategori');

$app->get('/posts', 'PostController@getPost');

$app->get('/post', 'PostController@getOnePost');

$app->get('/postkategori', 'PostController@getPostKategori');

$app->get('/comments', 'CommentsController@getComments');

$app->post('/login', 'UserController@login');

$app->post('/register', 'UserController@register');

$app->post('/addpost', 'PostController@addPost');

$app->post('/addkategori', 'KategoriController@addKategori');

$app->post('/addcomments', 'CommentsController@addComments');

$app->get('/edituser', 'UserController@edit');

$app->post('/updateuser', 'UserController@update');

$app->get('/editkategori', 'KategoriController@edit');

$app->post('/updatekategori', 'KategoriController@update');

$app->get('/editpost', 'PostController@edit');

$app->post('/updatepost', 'PostController@update');