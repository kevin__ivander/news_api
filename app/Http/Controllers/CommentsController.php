<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Illuminate\Support\Facades\DB;

use App\Comments;

use Auth;

class CommentsController extends Controller
{
    public function edit(Request $request)
    {
        $id = $request->input('id');
        $id1 = $request->input('id_post');
        $data = Comments::select('id','fullname','comment','id_post')->where('id_post',$id)->where('id',$id1)->get();

        if(count($data)!=null){
            $status = true;
            $status_code = 200;
            $message = 'data found';
            $value = $data;
        } else {
            $status = false;
            $status_code = 201;
            $message = 'data not found';
            $value = null;
        }

        $res = ['status' => $status,
                'status_code' => $status_code,
                'message' => $message,
                'respon' => $value
        ];

        return response()->json($res);
    }

    public function update(Request $request)
    {
        //
    }

    public function addComments(Request $request){

        $comment = new Comments();
        $comment->fullname = $request->input('fullname');
        $comment->comment = $request->input('comment');
        $comment->id_post = $request->input('id_post');

        $comment->save();
        $data = $comment;

        if(count($data)!=null){
            $status = true;
            $status_code = 200;
            $message = 'data succesfull saved';
            $value = $data;
        } else {
            $status = false;
            $status_code = 201;
            $message = 'data not succesfull saved';
            $value = null;
        }

        $res = ['status' => $status,
                'status_code' => $status_code,
                'message' => $message,
                'respon' => $value
        ];

        return response()->json($res);
    }

    public function getComments(Request $request){
        $id = $request->input('id');
        $data = Comments::select('id','fullname','comment','id_post')->where('id_post',$id)->orderBy('id_post','DESC')->paginate(5);

        if(count($data)!=null){
            $status = true;
            $status_code = 200;
            $message = 'data found';
            $value = $data;
        } else {
            $status = false;
            $status_code = 201;
            $message = 'data not found';
            $value = null;
        }

        $res = ['status' => $status,
                'status_code' => $status_code,
                'message' => $message,
                'respon' => $value
        ];

        return response()->json($res);
    }
}
