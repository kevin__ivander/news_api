<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Illuminate\Support\Facades\DB;

use App\User;

use Auth;

class UserController extends Controller
{
    public function edit(Request $request)
    {
        $id = $request->input('id');
        $data = User::select('id','name','email','username','password','jabatan')->where('id',$id)->get();

        if(count($data)!=null){
            $status = true;
            $status_code = 200;
            $message = 'data found';
            $value = $data;
        } else {
            $status = false;
            $status_code = 201;
            $message = 'data not found';
            $value = null;
        }

        $res = ['status' => $status,
                'status_code' => $status_code,
                'message' => $message,
                'respon' => $value
        ];

        return response()->json($res);
    }

    public function update(Request $request)
    {
        $id = $request->input('id');
        $user = User::where('id',$id)->first();
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->username = $request->input('username');
        $user->password = $request->input('password');
        $user->update();

        $data = $user;
        if(count($data)!=null){
            $status = true;
            $status_code = 200;
            $message = 'data found';
            $value = $data;
        } else {
            $status = false;
            $status_code = 201;
            $message = 'data not found';
            $value = null;
        }

        $res = ['status' => $status,
                'status_code' => $status_code,
                'message' => $message,
                'respon' => $value
        ];

        return response()->json($res);
    }

    public function register(Request $request){

        $user = new User();
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->username = $request->input('username');
        $user->password = $request->input('password');
        $user->jabatan = 'user';
        
        $image = $request->file('image');
        $fileName = $image->getClientOriginalName();
        $destinationPath = base_path() . '/public/image/' ;
        $image_url = url() . '/image/' . $fileName;
        $image->move($destinationPath, $fileName);
        $user->gambar = $image_url;

        $user->save();
        $data = $user;

        if(count($data)!=null){
            $status = true;
            $status_code = 200;
            $message = 'data succesfull saved';
            $value = $data;
        } else {
            $status = false;
            $status_code = 201;
            $message = 'data not succesfull saved';
            $value = null;
        }

        $res = ['status' => $status,
                'status_code' => $status_code,
                'message' => $message,
                'respon' => $value
        ];

        return response()->json($res);
    }

    public function login(Request $request){
        $username = $request->input('username');
        $password = $request->input('password');
        $data = User::select('id','gambar','name','email','username','password','jabatan')->where('username','=',$username)->where('password','=',$password)->get();

        if(count($data)!=null){
            $status = true;
            $status_code = 200;
            $message = 'data found';
            $value = $data;
        } else {
            $status = false;
            $status_code = 201;
            $message = 'data not found';
            $value = null;
        }

        $res = ['status' => $status,
                'status_code' => $status_code,
                'message' => $message,
                'respon' => $value
        ];

        return response()->json($res);
    }

    public function getUser(){
        $data = User::select('id','name','email','username','jabatan')->get();

        if(count($data)!=null){
            $status = true;
            $status_code = 200;
            $message = 'data found';
            $value = $data;
        } else {
            $status = false;
            $status_code = 201;
            $message = 'data not found';
            $value = null;
        }

        $res = ['status' => $status,
                'status_code' => $status_code,
                'message' => $message,
                'respon' => $value
        ];

        return response()->json($res);
    }

    public function getUserDetail(Request $request){
        $id = $request->input('id');
        $data = User::select('id','name','email','username','jabatan')->where('id',$id)->get();

        if(count($data)!=null){
            $status = true;
            $status_code = 200;
            $message = 'data found';
            $value = $data;
        } else {
            $status = false;
            $status_code = 201;
            $message = 'data not found';
            $value = null;
        }

        $res = ['status' => $status,
                'status_code' => $status_code,
                'message' => $message,
                'respon' => $value
        ];

        return response()->json($res);
    }
}
