<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Illuminate\Support\Facades\DB;

Use App\Post;

use App\Kategori;

use Auth;

class PostController extends Controller
{
    public function edit(Request $request)
    {
        $id = $request->input('id');
        $id1 = $request->input('id_post');
        $data = Post::select('id_post','judul_post','post','gambar','tanggal','id_kategori')->where('id_kategori',$id)->where('id_post',$id1)->get();

        if(count($data)!=null){
            $status = true;
            $status_code = 200;
            $message = 'data found';
            $value = $data;
        } else {
            $status = false;
            $status_code = 201;
            $message = 'data not found';
            $value = null;
        }

        $res = ['status' => $status,
                'status_code' => $status_code,
                'message' => $message,
                'respon' => $value
        ];

        return response()->json($res);
    }

    public function update(Request $request)
    {
        $id = $request->input('id_post');
        $post = Post::where('id_post',$id)->get();      
        $post->judul_post = $request->input('judul_post');
        $post->post = $request->input('post');
        $image = $request->file('image');
        $fileName = $image->getClientOriginalName();
        $destinationPath = base_path() . '/public/image/' ;
        $image_url = url() . '/image/' . $fileName;
        $image->move($destinationPath, $fileName);
        $post->gambar = $image_url;

        $kategori = $request->input('kategori');
        $getIdKategori = Kategori::select('id_kategori')->where('kategori',$kategori)->first();
        $post->tanggal = date('Y-m-d');
        $post->id_kategori = $getIdKategori->id_kategori;

        $post->update();
        $data = $post;


        if(count($data)!=null){
            $status = true;
            $status_code = 200;
            $message = 'data found';
            $value = $data;
        } else {
            $status = false;
            $status_code = 201;
            $message = 'data not found';
            $value = null;
        }

        $res = ['status' => $status,
                'status_code' => $status_code,
                'message' => $message,
                'respon' => $value
        ];

        return response()->json($res);
    }

    public function addPost(Request $request){
        
        $kategori = $request->input('kategori');
        $post = new Post();        
        $post->judul_post = $request->input('judul_post');
        $post->post = $request->input('post');
        $image = $request->file('image');
        $fileName = $image->getClientOriginalName();
        $destinationPath = base_path() . '/public/image/' ;
        $image_url = url() . '/image/' . $fileName;
        $image->move($destinationPath, $fileName);
        $post->gambar = $image_url;


        $getIdKategori = Kategori::select('id_kategori')->where('kategori',$kategori)->first();
        $post->tanggal = date('Y-m-d');
        $post->id_kategori = $getIdKategori->id_kategori;

        $post->save();
        $data = $post;

        if(count($data)!=null){
            $status = true;
            $status_code = 200;
            $message = 'data succesfull saved';
            $value = $data;
        } else {
            $status = false;
            $status_code = 201;
            $message = 'data not succesfull saved';
            $value = null;
        }

        $res = ['status' => $status,
                'status_code' => $status_code,
                'message' => $message,
                'respon' => $value
        ];

        return response()->json($res);
    }

    public function getPostKategori(Request $request){
        
        $id = $request->input('id');
        $data = Post::select('id_post','judul_post','post','gambar','tanggal','id_kategori')->where('id_kategori',$id)->get();

        if(count($data)!=null){
            $status = true;
            $status_code = 200;
            $message = 'data found';
            $value = $data;
        } else {
            $status = false;
            $status_code = 201;
            $message = 'data not found';
            $value = null;
        }

        $res = ['status' => $status,
                'status_code' => $status_code,
                'message' => $message,
                'respon' => $value
        ];

        return response()->json($res);
    }

    public function getOnePost(Request $request){

        $id = $request->input('id');
        $data = Post::select('id_post','judul_post','post','gambar','tanggal','id_kategori')->where('id_post',$id)->get();

        if(count($data)!=null){
            $status = true;
            $status_code = 200;
            $message = 'data found';
            $value = $data;
        } else {
            $status = false;
            $status_code = 201;
            $message = 'data not found';
            $value = null;
        }

        $res = ['status' => $status,
                'status_code' => $status_code,
                'message' => $message,
                'respon' => $value
        ];

        return response()->json($res);
    }

    public function getPost(){

        $data = Post::select('id_post','judul_post','post','gambar','tanggal','id_kategori')->orderBy('id_post','DESC')->paginate(10);

        if(count($data)!=null){
            $status = true;
            $status_code = 200;
            $message = 'data found';
            $value = $data;
        } else {
            $status = false;
            $status_code = 201;
            $message = 'data not found';
            $value = null;
        }

        $res = ['status' => $status,
                'status_code' => $status_code,
                'message' => $message,
                'respon' => $value
        ];

        return response()->json($res);
    }
}
