<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Illuminate\Support\Facades\DB;

use App\Kategori;

use Auth;

class KategoriController extends Controller
{
    public function edit(Request $request)
    {
        $id = $request->input('id');
        $data = Kategori::select('id_kategori','kategori')->where('id_kategori',$id)->get();

        if(count($data)!=null){
            $status = true;
            $status_code = 200;
            $message = 'data found';
            $value = $data;
        } else {
            $status = false;
            $status_code = 201;
            $message = 'data not found';
            $value = null;
        }

        $res = ['status' => $status,
                'status_code' => $status_code,
                'message' => $message,
                'respon' => $value
        ];

        return response()->json($res);
    }

    public function update(Request $request)
    {
        $id = $request->input('id_kategori');
        $kategori = Kategori::where('id_kategori',$id)->first();
        $kategori->kategori = $request->input('kategori');

        $kategori->update();
        $data = $kategori;

        if(count($data)!=null){
            $status = true;
            $status_code = 200;
            $message = 'data succesfull saved';
            $value = $data;
        } else {
            $status = false;
            $status_code = 201;
            $message = 'data not succesfull saved';
            $value = null;
        }

        $res = ['status' => $status,
                'status_code' => $status_code,
                'message' => $message,
                'respon' => $value
        ];

        return response()->json($res);
    }

    public function addKategori(Request $request){

        $kategori = new Kategori();
        $kategori->kategori = $request->input('kategori');

        $kategori->save();
        $data = $kategori;

        if(count($data)!=null){
            $status = true;
            $status_code = 200;
            $message = 'data succesfull saved';
            $value = $data;
        } else {
            $status = false;
            $status_code = 201;
            $message = 'data not succesfull saved';
            $value = null;
        }

        $res = ['status' => $status,
                'status_code' => $status_code,
                'message' => $message,
                'respon' => $value
        ];

        return response()->json($res);
    }

    public function getKategori(){
        $data = Kategori::select('id_kategori','kategori')->get();

        if(count($data)!=null){
            $status = true;
            $status_code = 200;
            $message = 'data found';
            $value = $data;
        } else {
            $status = false;
            $status_code = 201;
            $message = 'data not found';
            $value = null;
        }

        $res = ['status' => $status,
                'status_code' => $status_code,
                'message' => $message,
                'respon' => $value
        ];

        return response()->json($res);
    }
}
