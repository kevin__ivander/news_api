<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $table = 'post';
    protected $primaryKey = 'id_post';
    protected $fillable = ['judul_post',
    						'post',
    						'gambar',
    						'tanggal',
    						'id_kategori'];
    public $timestamps = false;
}
