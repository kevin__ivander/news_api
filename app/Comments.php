<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comments extends Model
{
    protected $table = 'comments';
    protected $primaryKey = 'id';
    protected $fillable = ['fullname',
    						'comment',
    						'id_post'];
    public $timestamps = false;
}